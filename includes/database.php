<?php

function aod_db_delta($sql)
{
    if (!function_exists('dbDelta')) {
        require ABSPATH.'wp-admin/includes/upgrade.php';
    }
    dbDelta($sql);
}

function aod_db_table_exists($table_name)
{
    global $wpdb;

    return $wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") === $table_name;
}

function aod_db_get_table_name($table_name = 'null')
{
    global $wpdb;

    return sprintf('%saod_%s', $wpdb->prefix, $table_name);
}

function aod_db_init()
{
    global $wpdb;
    if (!aod_db_table_exists($contacts_table_name = aod_db_get_table_name('contacts'))) {
        aod_db_delta("CREATE TABLE IF NOT EXISTS `{$contacts_table_name}`(
          id MEDIUMINT(11) NOT NULL AUTO_INCREMENT,

          email VARCHAR(255) NOT NULL,
          first_name VARCHAR(255),
          last_name VARCHAR(255),
          phone VARCHAR(20),
          fax VARCHAR(20),

          created_at DATETIME NOT NULL DEFAULT NOW(),
          updated_at DATETIME NOT NULL DEFAULT NOW(),

          PRIMARY KEY (id),
          KEY email (email(191))
        ) {$wpdb->get_charset_collate()}");
    }

    if (!aod_db_table_exists($entries_table_name = aod_db_get_table_name('entries'))) {
        aod_db_delta("CREATE TABLE IF NOT EXISTS `{$entries_table_name}` (
        id MEDIUMINT(11) NOT NULL AUTO_INCREMENT,
        
        contact_id MEDIUMINT(11) NOT NULL,
        entry_source VARCHAR(100) NOT NULL,
        entry_value LONGTEXT,

        created_at DATETIME NOT NULL DEFAULT NOW(),
        updated_at DATETIME NOT NULL DEFAULT NOW(),

        PRIMARY KEY (id),
        FOREIGN KEY (contact_id) REFERENCES {$contacts_table_name}(id)
      ) {$wpdb->get_charset_collate()}");
    }
}

add_action('plugins_loaded', 'aod_db_init');
