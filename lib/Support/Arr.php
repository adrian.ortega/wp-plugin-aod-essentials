<?php

namespace AOD\Support;

class Arr
{
    /**
     * Determines wheter the given value is array accessible.
     *
     * @param mixied $value
     *
     * @return bool
     */
    public static function accessible($value)
    {
        return is_array($value) || $value instanceof \ArrayAccess;
    }

    /**
     * Determine if the given key exists in the provided array.
     *
     * @param \ArrayAccess|array $arr
     * @param string|int         $key
     *
     * @return bool
     */
    public static function exists($arr, $key)
    {
        if ($arr instanceof \ArrayAccess) {
            return $arr->offsetExists($key);
        }

        if (is_float($key)) {
            $key = (string) $key;
        }

        return array_key_exists($key, $arr);
    }

    /**
     * Get a subset of the items from the given array.
     *
     * @param array        $array
     * @param array|string $keys
     *
     * @return array
     */
    public static function only($array, $keys)
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }

    /**
     * Return the first element in an array passing a given truth test.
     *
     * @param iterable $arr
     */
    public static function first(&$arr, ?callable $callable = null, $default = null)
    {
        if (is_null($callable)) {
            if (empty($arr)) {
                return value($default);
            }
            foreach ($arr as $item) {
                return $item;
            }
        }

        foreach ($arr as $key => $value) {
            if ($callable($value, $key)) {
                return $value;
            }
        }

        return value($default);
    }

    /**
     * Remove one or many array items from a given array using "dot" notation.
     *
     * @param array                  $arr
     * @param array|string|int|float $keys
     *
     * @return void
     */
    public static function forget(&$arr, $keys)
    {
        $original = &$arr;

        $keys = (array) $keys;

        if (count($keys) === 0) {
            return;
        }

        foreach ($keys as $key) {
            // if the exact key exists in the top-level, remove it
            if (static::exists($arr, $key)) {
                unset($arr[$key]);

                continue;
            }

            $parts = explode('.', $key);

            // clean up before each pass
            $arr = &$original;

            while (count($parts) > 1) {
                $part = array_shift($parts);

                if (isset($arr[$part]) && static::accessible($arr[$part])) {
                    $arr = &$arr[$part];
                } else {
                    continue 2;
                }
            }

            unset($arr[array_shift($parts)]);
        }
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param string|int|null $key
     */
    public static function get($arr, $key, $default = null)
    {
        if (!static::accessible($arr)) {
            return value($default);
        }

        if (is_null($key)) {
            return $arr;
        }

        if (static::exists($arr, $key)) {
            return $arr[$key];
        }

        if (!str_contains($key, '.')) {
            return $arr[$key] ?? value($default);
        }

        foreach (explode('.', $key) as $segment) {
            if (static::accessible($arr) && static::exists($arr, $segment)) {
                $arr = $arr[$segment];
            } else {
                return value($default);
            }
        }

        return $arr;
    }

    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param string|array $keys
     *
     * @return bool
     */
    public static function has($arr, $keys)
    {
        $keys = (array) $keys;

        if (!$arr || $keys === []) {
            return false;
        }

        foreach ($keys as $key) {
            $subKeyArray = $arr;

            if (static::exists($arr, $key)) {
                continue;
            }

            foreach (explode('.', $key) as $segment) {
                if (static::accessible($subKeyArray) && static::exists($subKeyArray, $segment)) {
                    $subKeyArray = $subKeyArray[$segment];
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Set an array item to a given value using "dot" notation.
     *
     * If no key is given to the method, the entire array will be replaced.
     *
     * @param string|int|null $key
     *
     * @return array
     */
    public static function set(&$arr, $key, $value)
    {
        if (is_null($key)) {
            return $arr = $value;
        }

        $keys = explode('.', $key);

        foreach ($keys as $i => $key) {
            if (count($keys) === 1) {
                break;
            }

            unset($keys[$i]);

            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if (!isset($arr[$key]) || !is_array($arr[$key])) {
                $arr[$key] = [];
            }

            $arr = &$arr[$key];
        }

        $arr[array_shift($keys)] = $value;

        return $arr;
    }

    /**
     * Filter the array using the given callback.
     *
     * @return array
     */
    public static function where($arr, callable $callback)
    {
        return array_filter($arr, $callback, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * Filter items where the value is not null.
     *
     * @param array $arr
     *
     * @return array
     */
    public static function whereNotNull($arr)
    {
        return static::where($arr, fn ($value) => !is_null($value));
    }

    /**
     * If the given value is not an array and not null, wrap it in one.
     *
     * @return array
     */
    public static function wrap($value)
    {
        if (is_null($value)) {
            return [];
        }

        return is_array($value) ? $value : [$value];
    }

    /**
     * Push an item to the beginning of an array.
     *
     * @param array $arr
     *
     * @return array
     */
    public static function prepend($arr, $value, $key = null)
    {
        if (func_num_args() == 2) {
            array_unshift($arr, $value);
        } else {
            $arr = [$key => $value] + $arr;
        }

        return $arr;
    }

    /**
     * Conditionally compile classes from an array into a CSS class list.
     *
     * @param array $array
     *
     * @return string
     */
    public static function toCssClasses($array)
    {
        $classList = static::wrap($array);
        $classes = [];
        foreach ($classList as $class => $constraint) {
            if (is_numeric($class)) {
                $classes[] = $constraint;
            } elseif ($constraint) {
                $classes[] = $class;
            }
        }

        return implode(' ', $classes);
    }

    /**
     * Get all of the given array except for a specified array of keys.
     *
     * @param array                  $arr
     * @param array|string|int|float $keys
     *
     * @return array
     */
    public static function except($arr, $keys)
    {
        static::forget($arr, $keys);

        return $arr;
    }

    /**
     * Flattens a multidimentional array.
     *
     * @return array
     */
    public static function flatten(array $array)
    {
        $result = [];
        array_walk_recursive(self::wrap($array), function ($a) use (&$result) {
            $result[] = $a;
        });

        return $result;
    }
}
