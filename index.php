<?php
/*
 * Plugin Name: AOD Essentials
 * Description: A Must-Use Plugin for any AOD website
 * Version: 1.0.0
 * Author: Adrian Ortega
 */

define('AOD_VER', '0.0.1');
define('AOD_ABSPATH', __DIR__);

// @TODO replace all of this with composer
//
define('AOD_INCLUDESPATH', AOD_ABSPATH.'/includes');
define('AOD_LIBPATH', AOD_ABSPATH.'/lib');

require_once AOD_INCLUDESPATH.'/helpers.php';

require_once AOD_LIBPATH.'/REST/Controllers/AbstractController.php';
require_once AOD_LIBPATH.'/REST/Controllers/ContactController.php';

require_once AOD_INCLUDESPATH.'/database.php';
require_once AOD_INCLUDESPATH.'/contacts.php';
require_once AOD_INCLUDESPATH.'/rest.php';
