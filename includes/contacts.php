<?php

function aod_create_contact($data)
{
    global $wpdb;
    $data = wp_parse_args($data, [
        'id' => -1,
        'first_name' => null,
        'last_name' => null,
        'email' => null,
        'phone' => null,
        'fax' => null,
    ]);
    if ($wpdb->insert(aod_db_get_table_name('contacts'), $data)) {
        $data['id'] = $wpdb->insert_id;
        $data['created'] = true;
    }

    return $data;
}

function aod_update_contact($contact_id, $data = [])
{
    global $wpdb;
    $data = wp_parse_args($data, ['updated_at' => date('Y-m-d H:i:s')]);

    return $wpdb->update(aod_db_get_table_name('contacts'), $data, ['id' => $contact_id]);
}

function aod_create_contact_entry($contact_id, $data = [], $source = 'default')
{
    global $wpdb;
    $entry = [
        'contact_id' => $contact_id,
        'entry_source' => $source,
        'entry_value' => json_encode($data),
    ];
    if ($wpdb->insert(aod_db_get_table_name('entries'), $entry)) {
        $entry['id'] = $wpdb->insert_id;
    } else {
        $entry['id'] = -1;
    }

    return $entry;
}

function aod_get_contact_by($key, $value)
{
    global $wpdb;
    $contacts_table = aod_db_get_table_name('contacts');

    return $wpdb->get_row(
        $wpdb->prepare("SELECT * FROM {$contacts_table} WHERE {$key} = %s", [$value]),
        ARRAY_A
    );
}
