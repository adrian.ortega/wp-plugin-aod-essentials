<?php

require_once AOD_LIBPATH.'/Support/Arr.php';

if (!function_exists('_dump_stringify')) {
    function _dump_stringify($arg)
    {
        if (is_int($arg)) {
            $arg = $arg = '<span style="color:#03a9f4">'.$arg.'</span>';
        } elseif (is_string($arg)) {
            $arg = '<span style="color:#4caf50;">'.$arg.'</span>';
        } elseif (is_null($arg)) {
            $arg = '<strong style="color:#ff9800">null</strong>';
        } elseif (is_bool($arg)) {
            $arg = $arg ? 'true' : 'false';
            $arg = '<strong style="color:#ff9800">'.$arg.'</strong>';
        }

        return $arg;
    }
}
if (!function_exists('dump')) {
    function dump($arg)
    {
        echo '<pre style="font-family:monospace;background-color: #202020; color: #f0f0f0;border: 1px solid #111;padding: 0.5em;border-radius: 4px;">';
        if (is_array($arg)) {
            $arg = array_map('_dump_stringify', $arg);
        } else {
            $arg = _dump_stringify($arg);
        }
        print_r($arg);
        echo '</pre>';
    }
}

if (!function_exists('dd')) {
    /**
     * For development purposes only, will dump anything passed into it.
     *
     * @return void
     */
    function dd()
    {
        foreach (func_get_args() as $arg) {
            dump($arg);
        }
        exit;
    }
}

if (!function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @source https://github.com/illuminate/collections/blob/master/helpers.php
     */
    function value($value, ...$args)
    {
        return $value instanceof Closure ? $value(...$args) : $value;
    }
}
