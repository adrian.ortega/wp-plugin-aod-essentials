<?php

namespace AOD\REST\Controllers;

use AOD\Support\Arr;

class ContactController extends AbstractController
{
    public function getEndpoints(): array
    {
        return [
            '/contact' => [
                'methods' => ['POST'],
                'callback' => [$this, 'create'],
                'args' => [
                    'email' => [
                        'type' => 'string',
                        'required' => true,
                    ],
                    'first_name' => [
                        'type' => 'string',
                    ],
                    'last_name' => [
                        'type' => 'string',
                    ],
                ],
            ],
            '/contact/get-started' => [
                'methods' => ['POST'],
                'callback' => [$this, 'getStarted'],
                'args' => [
                    'email' => [
                        'required' => true,
                    ],
                ],
            ],
        ];
    }

    public function getStarted(\WP_REST_Request $request)
    {
        $email = $request->get_param('email');
        if (empty($email)) {
            return rest_ensure_response(new \WP_Error(400, 'Missing Data'));
        }

        $contact = aod_get_contact_by('email', $email);
        if (empty($contact)) {
            $contact = aod_create_contact(['email' => $email]);
        }

        return $this->create_contact_entry_and_respond($contact, compact('email'), 'getting-started');
    }

    public function create(\WP_REST_Request $request)
    {
        // header('Content-Type: text/html');
        $data = $request->get_params();
        $contact_type = Arr::get($data, 'contact_type', 'default');
        $contact_data = Arr::only($data, ['first_name', 'last_name', 'email', 'phone']);

        if (!$contact = aod_get_contact_by('email', $contact_data['email'])) {
            $contact = aod_create_contact($contact_data);
        }

        if (isset($contact['id']) && !isset($contact['created'])) {
            aod_update_contact($contact['id'], $contact_data);
        }

        // There's no reason to store this data any more, in the entry
        //
        Arr::forget($data, ['first_name', 'last_name', 'email', 'phone', 'contact_type']);

        return $this->create_contact_entry_and_respond($contact, $data, $contact_type);
    }

    protected function create_contact_entry_and_respond(array $contact, $data, $type = 'default')
    {
        $eid = null;
        if (!empty($contact['id'])) {
            $entry = aod_create_contact_entry($contact['id'], $data, $type);
            $eid = $entry['id'];
        }

        return rest_ensure_response([
            'id' => $contact['id'],
            'e' => $eid,
        ]);
    }
}
