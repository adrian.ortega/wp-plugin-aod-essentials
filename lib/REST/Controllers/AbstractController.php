<?php

namespace AOD\REST\Controllers;

abstract class AbstractController
{
    abstract public function getEndpoints(): array;
}
