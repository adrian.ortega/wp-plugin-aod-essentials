<?php

use AOD\REST\Controllers\ContactController;

function aod_rest_endpoint_default()
{
    return 'default';
}

function aod_rest_get_endpoints()
{
    $endpoints = [];
    $controller = new ContactController();
    $endpoints += $controller->getEndpoints();

    return $endpoints;
}

function aod_rest_get_endpoint_namespace()
{
    return 'aod/v1';
}

function aod_rest_get_endpoint_path($args, $path)
{
    return isset($args['path']) ? $args['path'] : $path;
}

function aod_rest_permissions_check()
{
    return true; // current_user_can('edit_posts');
}

function aod_rest_get_endpoint_args($args)
{
    return wp_parse_args($args, [
        'methods' => ['GET'],
        'callback' => 'aod_rest_endpoint_default',
        'permission_callback' => 'aod_rest_permissions_check',
    ]);
}

function aod_rest_add_endpoints()
{
    foreach (aod_rest_get_endpoints() as $endpoint_path => $endpoint_args) {
        register_rest_route(
            aod_rest_get_endpoint_namespace(),
            aod_rest_get_endpoint_path($endpoint_args, $endpoint_path),
            aod_rest_get_endpoint_args($endpoint_args)
        );
    }
}
add_action('rest_api_init', 'aod_rest_add_endpoints');
